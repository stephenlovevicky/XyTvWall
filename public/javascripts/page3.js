$(document).ready(function () {
    const myChart = echarts.init(document.getElementById('chart'));
    const sjshA = echarts.init(document.getElementById('sjshA'));
    const sjshB = echarts.init(document.getElementById('sjshB'));
    const xjfqA = echarts.init(document.getElementById('xjfqA'));
    const xjfqB = echarts.init(document.getElementById('xjfqB'));

    const countUp = (el, num) => {
        return new CountUp(el, 0, num, 0, 2, {
            useEasing: true,
            useGrouping: true,
            separator: ',',
            decimal: '.'
        });
    };
    const ACount = countUp('ACount', 0);
    const BCount = countUp('BCount', 0);
    const getPieData = (percent, type, productName) => {
        let color;
        if (type === 'green') {
            color = new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                offset: 0,
                color: '#49ff25'
            }, {
                offset: 1,
                color: '#fff600'
            }])
        } else {
            color = new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                offset: 0,
                color: '#00a2ff'
            }, {
                offset: 1,
                color: '#78fffa'
            }])
        }

        if (type === 'green') {
            textColor1 = '#4fff24';
            textColor2 = '#32fa11';
        } else {
            textColor1 = '#6cf6fb';
            textColor2 = '#00d2ff';
        }

        return [{
            value: 1 - percent,
            name: percent,
            itemStyle: {
                normal: {
                    color: 'transparent'
                }
            },
            label: {
                normal: {
                    formatter: productName + '\n\n\n',
                    textStyle: {
                        color: textColor1,
                        fontSize: 18

                    }
                }
            }
        }, {
            value: percent,
            itemStyle: {
                normal: {
                    color: color
                }
            },
            label: {
                normal: {
                    textStyle: {
                        color: textColor2,
                        fontSize: 30
                    },
                    formatter: '\n{d} %'
                }
            }
        }];
    };

    const fetchData = () => {
        /*$.ajax({
            url: '/api/apply-product',
            method: 'GET'
        }).done(res => {*/
        const res = refreshMainProductData();
        const sjshTotal = res.xyqb.sjshCount + res.sjd.sjshCount;
        const xjfqTotal = res.xyqb.xjfqCount + res.sjd.xjfqCount;
        const sjshXyqbScale = (res.xyqb.sjshCount / sjshTotal).toFixed(2);
        const sjshSjdScale = (res.sjd.sjshCount / sjshTotal).toFixed(2);
        const xjfqXyqbScale = (res.xyqb.xjfqCount / xjfqTotal).toFixed(2);
        const xjfqSjdScale = (res.sjd.xjfqCount / xjfqTotal).toFixed(2);
        sjshA.setOption({
            series: [
                {
                    name: '享宇钱包',
                    type: 'pie',
                    radius: ['80%', '100%'],
                    avoidLabelOverlap: false,
                    label: {
                        normal: {
                            position: 'center'
                        }
                    },
                    data: getPieData(sjshXyqbScale, 'green', '享宇钱包')
                }
            ]
        });
        sjshB.setOption({
            series: [
                {
                    name: '移动手机贷',
                    type: 'pie',
                    radius: ['80%', '100%'],
                    avoidLabelOverlap: false,
                    label: {
                        normal: {
                            position: 'center'
                        }
                    },
                    data: getPieData(sjshSjdScale, 'green', '移动手机贷')
                }
            ]
        });
        xjfqA.setOption({
            series: [
                {
                    name: '享宇钱包',
                    type: 'pie',
                    radius: ['80%', '100%'],
                    avoidLabelOverlap: false,
                    label: {
                        normal: {
                            position: 'center'
                        }
                    },
                    data: getPieData(xjfqXyqbScale, 'blue', '享宇钱包')
                }
            ]
        });
        xjfqB.setOption({
            series: [
                {
                    name: '移动手机贷',
                    type: 'pie',
                    radius: ['80%', '100%'],
                    avoidLabelOverlap: false,
                    label: {
                        normal: {
                            position: 'center'
                        }
                    },
                    data: getPieData(xjfqSjdScale, 'blue', '移动手机贷')
                }
            ]
        });

        ACount.update(sjshTotal);
        BCount.update(xjfqTotal);
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption({
            grid: {
                show: false,
                left: 200,
                top: 600,
                right: 200,
                bottom: 100,
                containLabel: true
            },
            calculable: true,
            xAxis: [{
                type: 'category',
                boundaryGap: false,
                //在（type: 'category'）中设置data有效
                data: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24'],
                axisLine: {
                    lineStyle: {
                        color: '#fff'
                    }
                },
                splitLine: {
                    show: false
                }
            }],
            yAxis: [{
                type: 'value',
                axisLine: {
                    lineStyle: {
                        color: '#fff'
                    }
                },
                splitLine: {
                    show: false
                }
            }],
            series: [
                {
                    name: '随借随还',
                    type: 'line',
                    smooth: false, //是否平滑曲线显示
                    lineStyle: { //线条样式
                        normal: {
                            width: 1
                        }
                    },
                    areaStyle: { //区域填充样式
                        normal: {
                            //线性渐变，前4个参数分别是x0,y0,x2,y2(范围0~1);相当于图形包围盒中的百分比。如果最后一个参数是‘true’，则该四个值是绝对像素位置。
                            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                offset: 0,
                                color: 'rgba(215, 248, 8, 0.5)'
                            }, {
                                offset: 0.7,
                                color: 'rgba(95, 254, 33, 0.1)'
                            }], false),

                            shadowColor: 'rgba(0, 0, 0, 0.9)', //阴影颜色
                            shadowBlur: 10 //shadowBlur设图形阴影的模糊大小。配合shadowColor,shadowOffsetX/Y, 设置图形的阴影效果。
                        }
                    },
                    itemStyle: { //折现拐点标志的样式
                        normal: {
                            color: '#b4ff01'
                        }
                    },
                    data: res.sjsh.line,
                    // markLine: {
                    //     data: [
                    //         {type: 'average', name: '平均值'}
                    //     ]
                    // }
                },
                {
                    name: '现金分期',
                    type: 'line',
                    smooth: false,
                    lineStyle: {
                        normal: {
                            width: 1
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#00c4ff'
                        }
                    },
                    areaStyle: {
                        normal: {
                            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                offset: 0,
                                color: 'rgba(0,190,255, 0.4)'
                            }, {
                                offset: 0.8,
                                color: 'rgba(0,188,255, 0.1)'
                            }], false),
                            shadowColor: 'rgba(0, 0, 0, 0.9)',
                            shadowBlur: 10
                        }
                    },
                    data: res.xjfq.line,
                    // markLine: {
                    //     data: [
                    //         {type: 'average', name: '平均值'}
                    //     ]
                    // }
                }
            ] //series结束
        });
        //});
    };

    //init
    fetchData();
    const timer = window.setInterval(() => {
        fetchData();
    }, 1000 * 8);

    window.onunload = () => {
        window.clearInterval(timer);
    }
});


