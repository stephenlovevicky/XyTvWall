$(document).ready(function () {
    let loadCount = 0;
    const myMap = echarts.init(document.getElementById('map'));

    function countUp(el, num) {
        return new CountUp(el, 0, num, 0, 2, {
            useEasing: true,
            useGrouping: true,
            separator: ',',
            decimal: '.'
        });
    }

    const applyCount = countUp('applyCount', 0);
    const creditCount = countUp('creditCount', 298187666);

    // 申请列表滚动特效
    function doscroll() {
        const $parent = $('.list');
        const $first = $parent.find('li:first');
        const height = $first.height();
        $first.animate({
            height: 0
        }, 1000, () => {
            $first.css('height', height).appendTo($parent);// 动画结束后，把它插到最后，形成无缝
        });
    }

    // 替换为*号
    function transformString(string,startIndex,endIndex) {
        let str = '';
        if(endIndex < 0){
            str = string.substring(startIndex);
        }else{
            str = string.substring(startIndex,endIndex);
        }
        //console.log('=========str=========>',str);
        return string.replace(str,'*******');
        //return phone.toString().replace(/(\d{3})\d{4}(\d{4})/, '$1****$2');
    }

    // 更新系统时间
    function updateSystemTime() {
        $('.time').text(new Date().toLocaleString())
    }

    // 插入申请列表到页面
    function appendList() {
        for(var i=0;i<100;i++){
            let el = `<li class="item"><span class="phone">${transformString(getUserMobile(),3,10)}</span><span class="area">${refreshMainProvinceData()}</span></li>`;
            $('.list').prepend($(el));
        } // end of for
    }

    // 获取地图数据
    function fetchData() {
        refreshMainProductData();
        const maxApplyCount = refreshDayApplyCount();
        applyCount.update(maxApplyCount);
        creditCount.update(refreshCreditCount());
        /*$.ajax({
            url: '/api',
            method: 'GET'
        }).done(res => {*/
            /*if (loadCount === 1) {
                appendList(res.applyPeople);
            }*/
        const res = refreshMainMapData(maxApplyCount);
        /*const firstPeople = res.applyPeople && res.applyPeople[0];
        const dynamicPointData = [{name: firstPeople && firstPeople.province, value: [firstPeople && firstPeople.longitude, firstPeople && firstPeople.latitude], visualMap: false}];*/

        //applyCount.update(res.applyRegion.total);
        let temArr = topTenCityAry.sort((a, b) => {
            return b.value - a.value;
        });

        let showVal = randomNumBoth(0,cityNameAry.length-1);
        const firstPeople = geoCityDataMap[cityNameAry[showVal]];
        const dynamicPointData = [{name: cityNameAry[showVal], value: [firstPeople[0], firstPeople[1]], visualMap: false}];

        const topData = temArr.map(item => {
            return {name: item.name, value: item.value, visualMap: false}
        });
        const topName = temArr.map(item => {
            return item.name
        });

        // 使用刚指定的配置项和数据显示图表。
        myMap.setOption({
            visualMap: {
                min: 0,
                max: 1000,
                show: false,
                inRange: {
                    color: ['rgba(0,204,255,.1)', 'rgb(33,190,255)']
                },
                calculable: false
            },
            geo: {
                map: 'china',
                zoom: .9,
                left: '14%',
                regions: [{
                    name: '南海诸岛',
                    value: 0,
                    itemStyle: {
                        normal: {
                            opacity: 0,
                            label: {show: false}
                        }
                    },
                    label: {
                        normal: {
                            show: false
                        },
                        emphasis: {
                            show: false
                        }
                    }
                }],
                label: {
                    normal: {
                        show: false
                    },
                    emphasis: {
                        show: false
                    }
                },
                itemStyle: {
                    normal: {
                        borderColor: '#6aecff',
                        color: 'transparent'
                    }
                }
            },
            tooltip: {
                show: false
            },
            grid: {
                right: 100,
                top: 500,
                bottom: 100,
                width: '20%'
            },
            xAxis: {
                type: 'value',
                splitLine: {show: false},
                axisLabel: {show: false},
                axisTick: {show: false},
                axisLine: {show: false}
            },
            yAxis: {
                type: 'category',
                inverse: true,
                nameGap: 10,
                axisLine: {show: false},
                axisTick: {show: false},
                axisLabel: {interval: 0, margin: 10, textStyle: {color: '#fff'}},
                data: topName
            },
            series: [
                {
                    id: 'map',
                    type: 'map',
                    geoIndex: 0,
                    data: res.applyRegion.data
                },
                {
                    type: 'effectScatter',
                    coordinateSystem: 'geo',
                    data: dynamicPointData,
                    //symbol: 'image://./images/dianxin.svg',
                    symbolSize: 20,
                    showEffectOn: 'render',
                    rippleEffect: {
                        period: 4,
                        brushType: 'stroke',
                        scale: 10
                    },
                    hoverAnimation: true,
                    itemStyle: {
                        normal: {
                            color: 'orange'
                        }
                    },
                    zlevel: 2
                },
                {
                    type: 'scatter',
                    coordinateSystem: 'geo',
                    data: res.applyRegion.scatterData,
                    symbolSize: 50
                },
                {
                    id: 'bar',
                    type: 'bar',
                    itemStyle: {
                        normal: {
                            color: {
                                type: 'linear',
                                x: 0,
                                y: 0,
                                x2: 1,
                                y2: 0,
                                colorStops: [{
                                    offset: 0, color: 'rgba(0,192,255,.8)' // 0% 处的颜色
                                }, {
                                    offset: 1, color: 'rgba(41,167,255,.4)' // 100% 处的颜色
                                }]
                            }
                        }
                    },
                    tooltip: {
                        show: false
                    },
                    label: {
                        normal: {
                            show: true,
                            position: 'right',
                            textStyle: {
                                color: '#fff'
                            }
                        }
                    },
                    data: topData
                }
            ]
        });
        /*});
        loadCount++;*/
    }

    //init
    refreshMainTopTenData();
    fetchData();
    applyCount.start();
    creditCount.start();
    updateSystemTime();
    appendList();

    const timer1 = window.setInterval(() => {
        fetchData();
    }, 8 * 1000);
    const timer2 = window.setInterval(() => {
        doscroll()
    }, 6 * 1000);
    const timer3 = window.setInterval(() => {
        updateSystemTime();
    }, 1000);
    const timer4 = window.setInterval(() => {
        refreshMainTopTenData();
    }, 60 * 1000);

    window.onunload = () => {
        window.clearInterval(timer1);
        window.clearInterval(timer2);
        window.clearInterval(timer3);
        window.clearInterval(timer4);
    }

});


