$(document).ready(() => {
    let canvas = document.getElementById('canvas');
    let ctx = canvas.getContext('2d');
    let w = canvas.width = window.innerWidth;
    let h = canvas.height = window.innerHeight;
    let hue = 217;
    let stars = [];
    let count = 0;
    let maxStars = 1300;//星星数

    let canvas2 = document.createElement('canvas');
    let ctx2 = canvas2.getContext('2d');
    canvas2.width = 100;
    canvas2.height = 100;
    let half = canvas2.width / 2;
    let gradient2 = ctx2.createRadialGradient(half, half, 0, half, half, half);
    gradient2.addColorStop(0.025, '#CCC');
    gradient2.addColorStop(0.1, 'hsl(' + hue + ', 61%, 33%)');
    gradient2.addColorStop(0.25, 'hsl(' + hue + ', 64%, 6%)');
    gradient2.addColorStop(1, 'transparent');

    ctx2.fillStyle = gradient2;
    ctx2.beginPath();
    ctx2.arc(half, half, half, 0, Math.PI * 2);
    ctx2.fill();

    // End cache

    function random(min, max) {
        if (arguments.length < 2) {
            max = min;
            min = 0;
        }

        if (min > max) {
            let hold = max;
            max = min;
            min = hold;
        }

        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function maxOrbit(x, y) {
        let max = Math.max(x, y),
            diameter = Math.round(Math.sqrt(max * max + max * max));
        return diameter / 2;
        //星星移动范围，值越大范围越小，
    }

    let Star = function () {

        this.orbitRadius = random(maxOrbit(w, h));
        this.radius = random(60, this.orbitRadius) / 8;
        //星星大小
        this.orbitX = w / 2;
        this.orbitY = h / 2;
        this.timePassed = random(0, maxStars);
        this.speed = random(this.orbitRadius) / 500000;
        //星星移动速度
        this.alpha = random(2, 10) / 10;

        count++;
        stars[count] = this;
    };

    Star.prototype.draw = function () {
        let x = Math.sin(this.timePassed) * this.orbitRadius + this.orbitX,
            y = Math.cos(this.timePassed) * this.orbitRadius + this.orbitY,
            twinkle = random(10);

        if (twinkle === 1 && this.alpha > 0) {
            this.alpha -= 0.05;
        } else if (twinkle === 2 && this.alpha < 1) {
            this.alpha += 0.05;
        }

        ctx.globalAlpha = this.alpha;
        ctx.drawImage(canvas2, x - this.radius / 2, y - this.radius / 2, this.radius, this.radius);
        this.timePassed += this.speed;
    };

    for (let i = 0; i < maxStars; i++) {
        new Star();
    }

    function animation() {
        ctx.globalCompositeOperation = 'source-over';
        ctx.globalAlpha = 0.5; //尾巴
        ctx.fillStyle = 'hsla(' + hue + ', 64%, 6%, 2)';
        ctx.fillRect(0, 0, w, h);

        ctx.globalCompositeOperation = 'lighter';
        for (let i = 1, l = stars.length; i < l; i++) {
            stars[i].draw();
        }
        window.requestAnimationFrame(animation);
    }

    // canvas动画
    animation();

    function countUp(el, num) {
        return new CountUp(el, 0, num, 0, 2, {
            useEasing: true,
            useGrouping: true,
            separator: ',',
            decimal: '.'
        });
    }

    const channel1Count = countUp('channel1Count', 0); // 线下
    const channel2Count = countUp('channel2Count', 0); // 移动渠道
    const channel3Count = countUp('channel3Count', 0); // 线上
    const channel4Count = countUp('channel4Count', 0); // APP Store
    const channel5Count = countUp('channel5Count', 0); // 安卓市场

    function fetchData(isInit) {
        /*$.ajax({
            url: '/api/channel',
            method: 'GET'
        }).done(res => {*/
        let showVal = randomNumBoth(1,7);
        if(isInit)showVal = 5;
        //console.log('===========showVal=======>',showVal);
        const res = refreshMainChannelData();
        res.forEach(item => {
            switch (item.channel) {
                case '线下':
                    if(showVal > 0)channel1Count.update(item.count);
                    break;
                case '移动渠道':
                    if(showVal > 1)channel2Count.update(item.count);
                    break;
                case '线上':
                    if(showVal > 2)channel3Count.update(item.count);
                    break;
                case '苹果':
                    if(showVal > 3)channel4Count.update(item.count);
                    break;
                case '安卓市场':
                    if(showVal > 4)channel5Count.update(item.count);
                    break;
            }
        })
        //})
    }

    fetchData(true);

    const timer = window.setInterval(() => {
        fetchData(false)
    }, 1000 * 8);

    window.onunload = () => {
        window.clearInterval(timer);
    };

    window.onresize = () => {
        window.location.reload()
    }
});


