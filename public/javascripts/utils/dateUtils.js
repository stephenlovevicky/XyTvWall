function getCurFullYear() {
  var myDate = new Date();
  return myDate.getFullYear();
}

function getCurMonth() {
  var myDate = new Date();
  return myDate.getMonth() + 1;
}

function getCurDay() {
  var myDate = new Date();
  return myDate.getDate();
}

function getCurrentDate() {
  return formatDate(null, 'yyyy-MM-dd HH:mm:ss');
}

function getCurrentDateFormat(format) {
  if (!format)format = 'yyyy-MM-dd HH:mm:ss';
  return formatDate(null, format);
}

function getCurrentDateFormat2(date, format) {
  if (!format)format = 'yyyy-MM-dd HH:mm:ss';
  return formatDate(date, format);
}

// 时间天数加减格式化,days传正为加,传负为减
function getDateStrAddSubFormat(dateStr, days, format) {
  var dateAry = dateStr.split('-');
  return getDateAddSubFormat(new Date(dateAry[0], dateAry[1] - 1, dateAry[2]), days, format);
}

function getDateAddSubFormat(date, days, format) {
  if (!date)date = new Date();
  if (!format)format = 'yyyy-MM-dd HH:mm:ss';
  date.setDate(date.getDate() + days);
  return formatDate(date, format);
}

function compareTwoDateByFormat(beginTime, endTime) {
  var beginTimes = beginTime.substring(0, 10).split('-');
  var endTimes = endTime.substring(0, 10).split('-');
  beginTime = beginTimes[1] + '/' + beginTimes[2] + '/' + beginTimes[0] + ' ' + beginTime.substring(10, 19);
  endTime = endTimes[1] + '/' + endTimes[2] + '/' + endTimes[0] + ' ' + endTime.substring(10, 19);
  var a = (Date.parse(endTime) - Date.parse(beginTime)) / 3600 / 1000;

  if (a < 0) { // endTime < beginTime
    return 1;
  } else if (a > 0) { // endTime > beginTime
    return -1;
  } else if (a === 0) {
    return 0;
  } else {
    return 'exception';
  }
}

function formatDate(date, fmt) {
  if (date == null)date = new Date();
  var o = {
    'M+': date.getMonth() + 1,                 // 月份
    'd+': date.getDate(),                    // 日
    'H+': date.getHours(),                   // 小时
    'm+': date.getMinutes(),                 // 分
    's+': date.getSeconds(),                 // 秒
    'q+': Math.floor((date.getMonth() + 3) / 3), // 季度
    'S': date.getMilliseconds()             // 毫秒
  };

  if (/(y+)/.test(fmt))fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
  for (var k in o) if (new RegExp('(' + k + ')').test(fmt))fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)));
  return fmt;
}

function generateYearWeeks(year, isReverseAry) {
  const ONE_DAY = 24 * 3600 * 1000;
  var start = new Date(year, 0, 1);
  var end = new Date(year, 11, 31);
  var firstDay = start.getDay() || 7;
  var lastDay = end.getDay() || 7;
  var startTime = +start;
  var endTime = startTime + (7 - firstDay) * ONE_DAY;
  var _endTime = end - (7 - lastDay) * ONE_DAY;
  var resultAry = [];
  resultAry.push({dateS: startTime, dateE: endTime});
  startTime = endTime + ONE_DAY;
  endTime = endTime + 7 * ONE_DAY;
  while (endTime < _endTime) {
    resultAry.push({dateS: startTime, dateE: endTime});
    startTime = endTime + ONE_DAY;
    endTime = endTime + 7 * ONE_DAY;
  }
  resultAry.push({dateS: startTime, dateE: +end});
  return isReverseAry ? resultAry.reverse() : resultAry;
}

function formatDateNum(num) {
  return num > 9 ? '' + num : '0' + num;
}

function formatMillToDate(mill) {
  var y = new Date(mill);
  return {year: y.getFullYear(), month: formatDateNum(y.getMonth() + 1), day: formatDateNum(y.getDate()), week: y.getDay() || 7}
  // return String.raw({raw: [y.getFullYear(), formatDateNum(y.getMonth() + 1), formatDateNum(y.getDate()), y.getDay() || 7]}, ...['年', '月', '日 星期']);
}

function formatMillToDateRetStr(dateMill, fmt) {
  return formatDate(new Date(dateMill), fmt);
}

// 当月的第几周
function getMonthWeek(year, month, day) {
  var date = new Date(year, parseInt(month) - 1, day);
  var w = date.getDay();
  var d = date.getDate();
  return Math.ceil((d + 6 - w) / 7);
}

// 当年的第几周
function getYearWeek(year, month, day) {
  var date1 = new Date(year, parseInt(month) - 1, day);
  var date2 = new Date(year, 0, 1);
  var d = Math.round((date1.valueOf() - date2.valueOf()) / 86400000);
  return Math.ceil((d + ((date2.getDay() + 1) - 1)) / 7);
}

// 获取某个月的天数
function getDaysInOneMonth(year, month) {
  month = parseInt(month, 10);
  var d = new Date(year, month, 0);
  return d.getDate();
}