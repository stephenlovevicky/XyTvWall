(function () {
    let str = window.location.pathname;
    let url = '';
    if (str === '/') {
        url = '/page2'
    }
    if (str.charAt(str.length - 1) === '3') {
        url = '/'
    }
    if (str.charAt(str.length - 1) === '2') {
        url = '/page3'
    }
    setTimeout(() => {
        const curDay = getCurrentDateFormat('yyyy-MM-dd');
        const saveDay = sessionStorage.getItem("saveDayStr");
        //console.log('===========saveDay=======>',saveDay);
        //console.log('===========curDay=======>',curDay);
        //console.log('===========compare=======>',(curDay == saveDay));
        if(saveDay){
            if(saveDay == curDay){
                saveBaseData();
            }else{
                sessionStorage.setItem("saveDayStr",curDay);
                removeBaseData();
            }
        }else{
            sessionStorage.setItem("saveDayStr",curDay);
            saveBaseData();
        }
        location.href = url;
    }, 30 * 1000);
})();