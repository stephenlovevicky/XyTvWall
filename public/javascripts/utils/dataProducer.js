let geoCityDataMap = {},channelDataMap = {},topTenCityAry = [],cityNameAry = [],applyProductData = {xyqb: {},sjd: {},sjsh: {},xjfq: {}};
const iconArray = ['image://./images/yidong.svg', 'image://./images/liantong.svg', 'image://./images/dianxin.svg'];
//初始数据
function initBaseData() {
    const toDayTime = getToDayTime();
    const curTimeVal = (parseFloat(toDayTime)/parseFloat(24));

    let dayApplyCount = sessionStorage.getItem('dayApplyCount');
    if(!dayApplyCount || isNaN(parseInt(dayApplyCount)))sessionStorage.setItem('dayApplyCount',8);
    let creditCountData = sessionStorage.getItem('creditCount');
    if(!creditCountData || isNaN(parseInt(creditCountData)))sessionStorage.setItem('creditCount',298187666);

    const applyProductDataStr = sessionStorage.getItem('applyProductData');
    if(applyProductDataStr){
        applyProductData = JSON.parse(applyProductDataStr);
    }else{
        applyProductData.xyqb.sjshCount = ((curTimeVal*((parseFloat(1)/parseFloat(2))*24))+(toDayTime*parseFloat(parseFloat(300)/parseFloat(24))));
        applyProductData.sjd.sjshCount = ((curTimeVal*(3*24))+(toDayTime*parseFloat(parseFloat(300)/parseFloat(24))));
        applyProductData.xyqb.xjfqCount = ((curTimeVal*((parseFloat(4)/parseFloat(5))*24))+(toDayTime*parseFloat(parseFloat(300)/parseFloat(24))));
        applyProductData.sjd.xjfqCount = ((curTimeVal*((parseFloat(1)/parseFloat(5))*24))+(toDayTime*parseFloat(parseFloat(300)/parseFloat(24))));
        applyProductData.sjsh.line = [[1,12],[2,34],[3,45],[4,64],[5,32],[6,35],[7,56],[8,78],[9,97],[10,13],[11,33],[12,53],[13,23],[14,44],[15,66],[16,77],[17,55],[18,67],[19,78],[20,28],[21,34],[22,75],[23,44],[24,57]];
        applyProductData.xjfq.line = [[1,22],[2,54],[3,15],[4,24],[5,32],[6,56],[7,43],[8,24],[9,67],[10,89],[11,23],[12,12],[13,23],[14,23],[15,45],[16,67],[17,97],[18,22],[19,34],[20,24],[21,67],[22,32],[23,42],[24,22]];
        const sjshTotal = applyProductData.xyqb.sjshCount + applyProductData.sjd.sjshCount;
        const xjfqTotal = applyProductData.xyqb.xjfqCount + applyProductData.sjd.xjfqCount;
        sessionStorage.setItem('dayApplyCount',(sjshTotal+xjfqTotal));
        const sjshAry = randGenerator(toDayTime,sjshTotal);
        const xjfqAry = randGenerator(toDayTime,xjfqTotal);
        for(let i=0;i<applyProductData.sjsh.line.length;i++){
            let item = applyProductData.sjsh.line[i];
            if(i < sjshAry.length){
                item[1]=sjshAry[i];
            }else{
                item[1]=0;
            }
        } // end of for
        for(let i=0;i<applyProductData.xjfq.line.length;i++){
            let item = applyProductData.xjfq.line[i];
            if(i < xjfqAry.length){
                item[1]=xjfqAry[i];
            }else{
                item[1]=0;
            }
        } // end of for
    }

    const geoCityDataMapStr = sessionStorage.getItem('geoCityDataMap');
    if(geoCityDataMapStr){
        geoCityDataMap = JSON.parse(geoCityDataMapStr);
    }else{
        geoCityDataMap = {//省份后面依次是:经度/维度/申请数/图标索引
            '重庆市': [106.54, 29.59, 0, 2],
            '重庆綦江': [106.56, 29.41, 0, 0],
            '重庆潼南': [106.22, 30.03, 0, 0],
            '重庆铜梁': [105.8, 30.16, 0, 2],
            '重庆永川': [105.71, 29.75, 0, 2],
            '重庆万盛': [105.91, 29.38, 0, 0],

            '浙江杭州': [120.19, 30.26, 0, 0],
            '浙江余杭': [120.3, 30.43, 0, 0],
            '浙江常山': [118.5, 28.9, 0, 1],

            '云南威信': [105.05, 27.85, 0, 1],
            '云南镇雄': [104.86, 27.42, 0, 0],
            '云南沽益': [103.82, 25.62, 0, 2],
            '云南易门': [102.15, 24.67, 0, 2],
            '云南元江': [102, 23.59, 0, 1],

            /*'新疆乌鲁木齐': [87.68, 43.77, 0, 1],
            '新疆克拉玛依': [84.77, 45.59, 0, 0],
            '新疆鄯善': [90.25, 42.82, 0, 0],
            '新疆伊吾': [94.65, 43.28, 0, 2],
            '新疆巴里坤': [93, 43.6, 0, 2],
            '新疆哈巴河': [86.41, 48.05, 0, 2],

            '西藏拉萨': [91.11, 29.97, 0, 1],
            '西藏林周': [91.24, 30.2, 0, 2],
            '西藏米林': [94.13, 29.18, 0, 0],
            '西藏加查': [92.6, 29.09, 0, 0],
            '西藏桑日': [92, 29.26, 0, 2],
            '西藏普兰': [81.18, 30.37, 0, 0],*/

            '北京市': [116.46, 39.92, 0, 1],
            '北京平谷': [117.1, 40.13, 0, 2],
            '北京大兴': [116.33, 39.73, 0, 0],
            '北京房山': [115.98, 39.72, 0, 1],
            '北京昌平': [116.2, 40.22, 0, 1],

            '安徽合肥': [117.27, 31.86, 0, 1],
            '安徽长丰': [117.16, 32.47, 0, 2],
            '安徽铜陵': [117.82, 30.93, 0, 0],
            '安徽界首': [115.34, 33.24, 0, 1],

            '福建福州': [119.3, 26.08, 0, 1],
            '福建闽侯': [119.14, 26.16, 0, 0],
            '福建明溪': [117.18, 26.36, 0, 1],

            '甘肃兰州': [103.73, 36.03, 0, 1],
            '甘肃徽县': [106.11, 33.78, 0, 0],
            '甘肃阿克塞': [94.25, 38.46, 0, 1],

            '广东广州': [113.23, 23.16, 0, 0],
            '广东花县': [113.19, 23.4, 0, 1],
            '广东德庆': [111.75, 23.15, 0, 2],

            '广西南宁': [108.33, 22.84, 0, 1],
            '广西河池': [108.06, 24.7, 0, 2],
            '广西田林': [106.24, 24.31, 0, 1],

            '贵州贵阳': [106.71, 26.57, 0, 1],
            '贵州六盘水': [104.82, 26.58, 0, 1],
            '贵州绥阳': [107.19, 27.95, 0, 2],

            '海南海口': [110.35, 20.02, 0, 1],
            '海南万宁': [110.39, 18.8, 0, 2],
            '海南东方': [108.64, 19.09, 0, 1],

            '河北石家庄': [114.48, 38.03, 0, 1],
            '河北行唐': [114.54, 38.42, 0, 0],
            '河北故城': [115.96, 37.36, 0, 1],

            '河南郑州': [113.65, 34.76, 0, 1],
            '河南荥阳': [113.35, 34.79, 0, 0],
            '河南兰考': [114.81, 34.69, 0, 0],

            '黑龙江哈尔滨': [126.63, 45.75, 0, 1],
            '黑龙江齐齐哈尔': [123.97, 47.33, 0, 0],
            '黑龙江伊春': [128.92, 47.73, 0, 2],
            '黑龙江塔河': [124.7, 52.32, 0, 1],

            '湖北武汉': [114.31, 30.52, 0, 1],
            '湖北武昌': [114.33, 30.35, 0, 2],
            '湖北石首': [112.41, 29.73, 0, 1],

            '湖南长沙': [113, 28.21, 0, 1],
            '湖南望城': [112.8, 28.37, 0, 0],
            '湖南安化': [111.2, 28.38, 0, 1],

            '吉林长春': [125.35, 43.88, 0, 1],
            '吉林吉林': [126.57, 43.87, 0, 1],

            '江苏南京': [118.78, 32.04, 0, 1],
            '江苏江宁': [118.83, 31.95, 0, 1],

            '江西南昌': [115.89, 28.68, 0, 1],
            '江西大余': [114.36, 25.39, 0, 2],

            '上海市': [121.48, 31.22, 0, 0],
            '上海嘉定': [121.24, 31.4, 0, 1],
            '上海南汇': [121.76, 31.05, 0, 2],

            '四川成都': [104.06, 30.67, 0, 1],
            '四川双流': [104.94, 30.57, 0, 0],
            '四川蒲江': [103.29, 30.2, 0, 1],
            '四川米易': [102.15, 26.9, 0, 0],
        };
    }

    const topTenCityStr = sessionStorage.getItem('topTenCityStr');
    if(topTenCityStr){
        topTenCityAry = JSON.parse(topTenCityStr);
    }else{
        topTenCityAry = [{name:"重庆",value:0},{name:"浙江",value:0},{name:"四川",value:0},{name:"上海",value:0},{name:"江苏",value:0},{name:"湖北",value:0},{name:"河北",value:0},{name:"广东",value:0},{name:"北京",value:0},{name:"云南",value:0}];
        let isNeedRepeat = false,topTenValAry = randGenerator(10,parseInt(sessionStorage.getItem('dayApplyCount'))),curCount = 0,maxCount = 20;
        do{
            isNeedRepeat = false;
            console.log('===========topTenCityAry=======>',topTenValAry);
            for(let i=0;i<topTenValAry.length;i++){
                const index = topTenValAry.indexOf(topTenValAry[i]);
                if(index >= 0 && index != i && curCount < maxCount){
                    console.log('============包含重复的值,需重新计算随机数======>',curCount,topTenValAry[i]);
                    topTenValAry = randGenerator(10,parseInt(sessionStorage.getItem('dayApplyCount')));
                    curCount++;
                    isNeedRepeat = true;
                    break;
                } // end of if
            } // end of for
        }while(isNeedRepeat);
        curCount = 0;
        console.log('==================>随机数组检查重复完毕!');
        if(topTenValAry && topTenCityAry.length == topTenValAry.length)for(let i=0;i<topTenValAry.length;i++)topTenCityAry[i].value = parseInt(topTenValAry[i]);
    }

    const channelDataMapStr = sessionStorage.getItem('channelDataMap');
    if(channelDataMapStr){
        channelDataMap = JSON.parse(channelDataMapStr);
    }else{
        channelDataMap = {
            '线下': 100,
            '移动渠道': 121,
            '线上': 55,
            '苹果': 322,
            '安卓市场': 251
        };
    }
}

//保存数据
function saveBaseData() {
    //console.log('=========saveBaseData=========>');
    sessionStorage.setItem('geoCityDataMap',JSON.stringify(geoCityDataMap));
    sessionStorage.setItem('channelDataMap',JSON.stringify(channelDataMap));
    sessionStorage.setItem('applyProductData',JSON.stringify(applyProductData));
    sessionStorage.setItem('topTenCityStr',JSON.stringify(topTenCityAry));
}

//清除数据
function removeBaseData() {
    //console.log('=========removeBaseData=========>');
    sessionStorage.removeItem('dayApplyCount');
    sessionStorage.removeItem('creditCount');
    sessionStorage.removeItem('geoCityDataMap');
    sessionStorage.removeItem('channelDataMap');
    sessionStorage.removeItem('applyProductData');
    sessionStorage.removeItem('topTenCityStr');
}

//随机值生成器,baseVal是范围基础值,比如获取范围是(1~10,baseVal就是1)(10~20,baseVal就是10)
function randomGenerator(baseVal,isNeedPlusMinus) {
    let randomVal = (parseInt(Math.floor(Math.random()*10+baseVal)) * (isNeedPlusMinus ? (randomPlusMinusGenerator() ? 1 : -1) : 1));
    //console.log('==========1========>',randomVal);
    if(0 == baseVal)randomVal = (randomVal > 2 ? 0 : parseInt(randomVal/2));
    //console.log('==========2========>',randomVal);
    return randomVal;
}

//正负方向随机生成器
function randomPlusMinusGenerator() {
    const retVal = (0 == (parseInt(Math.random()*10) % 2));
    //console.log('========randomPlusMinusGenerator==========>',retVal);
    return retVal;
}

//随机某个范围
function randomNumBoth(Min,Max){
    var Range = Max - Min;
    var Rand = Math.random();
    var num = Min + Math.round(Rand * Range); //四舍五入
    return num;
}

//今日申请数
function refreshDayApplyCount() {
    let refreshData = sessionStorage.getItem('dayApplyCount');
    refreshData = parseInt(refreshData);
    /*refreshData += randomGenerator(0);
    sessionStorage.setItem('dayApplyCount',refreshData);*/
    return refreshData;
}

//覆盖人数
function refreshCreditCount() {
    let refreshData = sessionStorage.getItem('creditCount');
    refreshData = parseInt(refreshData);
    refreshData += randomGenerator(0);
    sessionStorage.setItem('creditCount',refreshData);
    return refreshData;
}

//申请人数-主地图数据
function refreshMainMapData(maxApplyCount) {
    let retJson = {};
    let regionAry = [],scatterData = [],curCount = 0,index = 0;
    cityNameAry = [];
    for (let item in geoCityDataMap) {
        let geoObj = geoCityDataMap[item];
        let cityVal = geoObj[2] + randomGenerator(0);
        curCount += cityVal;
        //console.log('==========refreshMainMapData========>',curCount,maxApplyCount);
        if(curCount <= maxApplyCount){
            geoObj[2] = cityVal;
            regionAry.push({name: item, value: cityVal});
        }else{
            regionAry.push({name: item, value: geoObj[2]});
        }
        //console.log('==================>',geoObj,geoObj[3]);
        scatterData.push({name: item + '', value: [geoObj[0], geoObj[1]], symbol: iconArray[geoObj[3]]});

        cityNameAry[index] = item;
        index++;
    } // end of for
    retJson.applyRegion = {total: 100, data: regionAry, scatterData: scatterData};
    return retJson;
}

//申请省份随机数据
function refreshMainProvinceData() {
    let regionAry = [];
    for (let item in geoCityDataMap)regionAry.push(item);
    return regionAry[randomNumBoth(0,regionAry.length-1)];
}

//渠道对应数据
function refreshMainChannelData() {
    let retJson = [];
    for (let item in channelDataMap){
        let cityVal = channelDataMap[item] + randomGenerator(1,true);
        if(cityVal > 0)channelDataMap[item] = cityVal;
        retJson.push({channel: item, count: channelDataMap[item]});
    } // end of for
    return retJson;
}

//申请人数-产品占比数据
function refreshMainProductData() {
    let retJson = {xyqb: {},sjd: {},sjsh: {line: []},xjfq: {line: []}};

    const sjshTotal1 = applyProductData.xyqb.sjshCount + applyProductData.sjd.sjshCount;
    const xjfqTotal1 = applyProductData.xyqb.xjfqCount + applyProductData.sjd.xjfqCount;

    retJson.xyqb.sjshCount = applyProductData.xyqb.sjshCount;
    applyProductData.xyqb.sjshCount += randomGenerator(0);
    retJson.sjd.sjshCount = applyProductData.sjd.sjshCount;
    applyProductData.sjd.sjshCount += randomGenerator(0);
    retJson.xyqb.xjfqCount = applyProductData.xyqb.xjfqCount;
    applyProductData.xyqb.xjfqCount += randomGenerator(0);
    retJson.sjd.xjfqCount = applyProductData.sjd.xjfqCount;
    applyProductData.sjd.xjfqCount += randomGenerator(0);

    let toDayTime = getToDayTime();
    const sjshTotal2 = applyProductData.xyqb.sjshCount + applyProductData.sjd.sjshCount;
    const xjfqTotal2 = applyProductData.xyqb.xjfqCount + applyProductData.sjd.xjfqCount;
    sessionStorage.setItem('dayApplyCount',(sjshTotal2+xjfqTotal2));
    /*console.log('=========1=========>',sjshTotal1,xjfqTotal1);
    console.log('=========2=========>',sjshTotal2,xjfqTotal2);
    console.log('=========3=========>',sjshTotal2-sjshTotal1,xjfqTotal2-xjfqTotal1);*/

    toDayTime-=2;
    for(let i=0;i<applyProductData.sjsh.line.length;i++){
        if(i > toDayTime)break;
        let item = applyProductData.sjsh.line[i];
        if(i == toDayTime){
            let itemVal = item[1];
            itemVal += (sjshTotal2-sjshTotal1);
            item[1] = itemVal;
        } // end of if
        retJson.sjsh.line.push(item);
        /*retJson.sjsh.line.push(item);
        let itemVal = item[1];
        itemVal += randomGenerator(0,true);
        if(itemVal > 0)item[1] = itemVal;*/
    } // end of for
    for(let i=0;i<applyProductData.xjfq.line.length;i++){
        if(i > toDayTime)break;
        let item = applyProductData.xjfq.line[i];
        if(i == toDayTime){
            let itemVal = item[1];
            itemVal += (xjfqTotal2-xjfqTotal1);
            item[1] = itemVal;
        } // end of if
        retJson.xjfq.line.push(item);
        /*retJson.xjfq.line.push(item);
        let itemVal = item[1];
        itemVal += randomGenerator(0,true);
        if(itemVal > 0)item[1] = itemVal;*/
    } // end of for
    return retJson;
}

function refreshMainTopTenData() {
    const valIndex = Math.floor(Math.random()*10);
    console.log('========refreshMainTopTenData======valIndex====>',valIndex);
    if(valIndex < topTenCityAry.length)topTenCityAry[valIndex].value += parseInt(parseFloat(valIndex)/parseFloat(3));
    /*let topTenVal2Ary = randGenerator(10,(sjshTotal2-sjshTotal1)+(xjfqTotal2-xjfqTotal1));
    if(topTenVal2Ary && topTenVal2Ary.length == topTenCityAry.length){
        topTenVal2Ary = topTenVal2Ary.reverse();
        for(let i=0;i<topTenVal2Ary.length;i++)topTenCityAry[i].value += parseInt(topTenVal2Ary[i]);
    } // end of if*/
}

//获得今天的当前小时
function getToDayTime() {
    let toDayTime = (new Date()).getHours();
    if(0 == toDayTime)toDayTime = 24;
    //console.log('=========toDayTime=========>',toDayTime);
    return toDayTime;
}

//生成n个和为固定sum的随机整数
function randGenerator(n, sum) {
    var aryRet = [];
    var fSumTmp = sum;
    var iAcc = 0;
    for (var i = 0; i < (n -1); i++) {
        var iTmp = Math.ceil(Math.random() * (fSumTmp / 2));
        aryRet.push(iTmp);
        fSumTmp -= iTmp;
        iAcc += iTmp;
    } // end of for
    aryRet.push(sum-iAcc);
    return aryRet;
}

// 生成随机姓名
function getUserName(){
    let familyNames = new Array(
        "赵",  "钱",  "孙",  "李",  "周",  "吴",  "郑",  "王",  "冯",  "陈",
        "褚",  "卫",  "蒋",  "沈",  "韩",  "杨",  "朱",  "秦",  "尤",  "许",
        "何",  "吕",  "施",  "张",  "孔",  "曹",  "严",  "华",  "金",  "魏",
        "陶",  "姜",  "戚",  "谢",  "邹",  "喻",  "柏",  "水",  "窦",  "章",
        "云",  "苏",  "潘",  "葛",  "奚",  "范",  "彭",  "郎",  "鲁",  "韦",
        "昌",  "马",  "苗",  "凤",  "花",  "方",  "俞",  "任",  "袁",  "柳",
        "酆",  "鲍",  "史",  "唐",  "费",  "廉",  "岑",  "薛",  "雷",  "贺",
        "倪",  "汤",  "滕",  "殷",  "罗",  "毕",  "郝",  "邬",  "安",  "常",
        "乐",  "于",  "时",  "傅",  "皮",  "卞",  "齐",  "康",  "伍",  "余",
        "元",  "卜",  "顾",  "孟",  "平",  "黄",  "和",  "穆",  "萧",  "尹"
    );
    let givenNames = new Array(
        "子璇", "淼", "国栋", "夫子", "瑞堂", "甜", "敏", "尚", "国贤", "贺祥", "晨涛",
        "昊轩", "易轩", "益辰", "益帆", "益冉", "瑾春", "瑾昆", "春齐", "杨", "文昊",
        "东东", "雄霖", "浩晨", "熙涵", "溶溶", "冰枫", "欣欣", "宜豪", "欣慧", "建政",
        "美欣", "淑慧", "文轩", "文杰", "欣源", "忠林", "榕润", "欣汝", "慧嘉", "新建",
        "建林", "亦菲", "林", "冰洁", "佳欣", "涵涵", "禹辰", "淳美", "泽惠", "伟洋",
        "涵越", "润丽", "翔", "淑华", "晶莹", "凌晶", "苒溪", "雨涵", "嘉怡", "佳毅",
        "子辰", "佳琪", "紫轩", "瑞辰", "昕蕊", "萌", "明远", "欣宜", "泽远", "欣怡",
        "佳怡", "佳惠", "晨茜", "晨璐", "运昊", "汝鑫", "淑君", "晶滢", "润莎", "榕汕",
        "佳钰", "佳玉", "晓庆", "一鸣", "语晨", "添池", "添昊", "雨泽", "雅晗", "雅涵",
        "清妍", "诗悦", "嘉乐", "晨涵", "天赫", "玥傲", "佳昊", "天昊", "萌萌", "若萌"
    );
    let i = parseInt(10 * Math.random())*10 + parseInt(10 * Math.random());
    let familyName = familyNames[i];
    let j = parseInt(10 * Math.random())*10 + parseInt(10 * Math.random());
    let givenName = givenNames[i];
    let name = familyName + givenName;
    return name;
}
//生成随机手机号
function getUserMobile() {
    let prefixArray = new Array("130", "131", "132", "133", "135", "137", "138", "170", "187", "189");
    let i = parseInt(10 * Math.random());
    let prefix = prefixArray[i];
    for (let j = 0; j < 8; j++)prefix = prefix + Math.floor(Math.random() * 10);
    return prefix;
}
// 生成随机身份证号
function getUserIdNo(){
    let coefficientArray = [ "7","9","10","5","8","4","2","1","6","3","7","9","10","5","8","4","2"];// 加权因子
    let lastNumberArray = [ "1","0","X","9","8","7","6","5","4","3","2"];// 校验码
    let address = "420101"; // 住址
    let birthday = "19810101"; // 生日
    let s = Math.floor(Math.random()*10).toString() + Math.floor(Math.random()*10).toString() + Math.floor(Math.random()*10).toString();
    let array = (address + birthday + s).split("");
    let total = 0;
    for(i in array)total = total + parseInt(array[i])*parseInt(coefficientArray[i]);
    let lastNumber = lastNumberArray[parseInt(total%11)];
    let id_no_String = address + birthday + s + lastNumber;
    return id_no_String;
}

//init
initBaseData();