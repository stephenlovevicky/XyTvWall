const express = require('express');
const router = express.Router();
const Apply = require('../controllers/apply');

router.get('/', Apply.getApplyData);
router.get('/apply-product', Apply.getApplyProduct);
router.get('/channel', Apply.getApplyChannel);

module.exports = router;
