let express = require('express');
let router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('page1', {title: 'page1'});
});
router.get('/page2', function (req, res, next) {
    res.render('page2', {title: 'page2'});
});
router.get('/page3', function (req, res, next) {
    res.render('page3', {title: 'page3'});
});

module.exports = router;
